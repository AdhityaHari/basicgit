<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Latihan Looping</h1>
    <?php
    echo "<h3>Soal 1</h3>";
    echo "<h4>Looping 1</h4>";
    for($x=2; $x<=20; $x+=2){
        echo $x . " - I Love PHP <br>" ;
    }

    echo "<h4>Looping 2</h4>";
    for($y=20; $y>=2; $y-=2){
        echo $y . " - I Love PHP <br>";
    }

    echo "<h3>Soal 2</h3>";
    $number = [18, 45, 29, 61, 47, 34];
    echo " Array number : ";
    print_r($number);
    echo "<br>";
    echo "Hasil sisa dari array Number ";
    foreach($number as $value){
        $set[] = $value %= 6;
    }
    print_r($set);

    echo "<h3>Soal 3</h3>";
    $barang = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach($barang as $key => $value){
        $item = array(
            "id" => $value[0],
            "name" => $value[1],
            "price" => $value[2],
            "description" => $value[3],
            "source" => $value[4]
        );
        print_r($item);
        echo "<br>";
    }

    echo "<h3>Soal 4</h3>";
    for($i=1; $i<=7; $i++){
        for($j=1; $j<=$i; $j++){
            echo "*";
        }
        echo "<br>";
    }
    ?>
</body>
</html>