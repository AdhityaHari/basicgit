<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Konditional</h1>
    <h1>Berlatih Function PHP</h1>
    <?php

        echo "<h3> Soal No 1 Greetings </h3>";
        /* 
        Soal No 1
        Greetings
        Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

        contoh: greetings("abduh");
        Output: "Halo Abduh, Selamat Datang di Sanbercode!"
        */

        // Code function di sini
        function salam($nama){
            echo "Halo " . $nama . " Selamat datang di Sanbercode! <br>";
        }

        // Hapus komentar untuk menjalankan code!
        salam("Bagas");
        salam("Wahyu");
        salam("Abdul");

        echo "<br>";

        echo "<h3>Soal No 2 Reverse String</h3>";
        /* 
        Soal No 2
        Reverse String
        Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
        Function reverseString menerima satu parameter berupa string.
        NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

        reverseString("abdul");
        Output: ludba

        */

        // Code function di sini 
        function reverse($kata){
            $lengthkata = strlen($kata);
            $temp = "";
            for($x=($lengthkata - 1); $x>=0; $x--){
                $temp .= $kata[$x];
            }
            return $temp;
        }

        function reverseString($kata2){
            $string = reverse($kata2);
            echo $string . "<br>";
        }

        // Hapus komentar di bawah ini untuk jalankan Code
        reverseString("abduh");
        reverseString("Sanbercode");
        reverseString("We Are Sanbers Developers");
        echo "<br>";


        

        echo "<h3>Soal No 3 Palindrome </h3>";
        /* 
        Soal No 3 
        Palindrome
        Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
        Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
        Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
        NB: 
        Contoh: 
        palindrome("katak") => output : "true"
        palindrome("jambu") => output : "false"
        NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

        */


        // Code function di sini
        function palindrome($kalimat){
            $revKalimat = reverse($kalimat);
            if($kalimat === $revKalimat){
                echo $kalimat . " => True <br>";
            }else{
                echo $kalimat . " => False <br>";
            }
        }

        // Hapus komentar di bawah ini untuk jalankan code
        palindrome("civic") ; // true
        palindrome("nababan") ; // true
        palindrome("jambaban"); // false
        palindrome("racecar"); // true
        echo "<br>";

        echo "<h3>Soal No 4 Tentukan Nilai </h3>";
        /*
        Soal 4
        buatlah sebuah function bernama hitungNilai . Di dalam function hitungNilai yang menerima parameter 
        berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
        Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
        sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
        */

        // Code function di sini
        function hitungNilai($angka){
            $hasil = "";
            if($angka>=85 && $angka<100){
                $hasil .= "Sangat Baik";
            }else if($angka>=70 && $angka<85){
                $hasil .= "Baik";
            }else if($angka>=55 && $angka<70){
                $hasil .= "Cukup";
            }else if($angka>=35 && $angka<55){
                $hasil .= "Kurang";
            }
            return $hasil . "<br>";
        }


        // Hapus komentar di bawah ini untuk jalankan code
        echo hitungNilai(98); //Sangat Baik
        echo hitungNilai(76); //Baik
        echo hitungNilai(67); //Cukup
        echo hitungNilai(43); //Kurang
    ?>
</body>
</html>