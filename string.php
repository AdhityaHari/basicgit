<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh soal</h1>
    <?php
        echo "<h3>Soal 1</h3>";
        $kalimat_1 = "PHP is never old";
        echo "Kalimat pertama : ". $kalimat_1 . "<br>";
        echo "Panjang String : ". strlen($kalimat_1). "<br>";
        echo "Jumlah kata : ". str_word_count($kalimat_1). "<br><br>";

        echo "<h3>Soal 2</h3>";
        $kalimat_2 = "I love PHP";
        echo "Kalimat kedua : ". $kalimat_2 . "<br>";
        echo "kata pertama : ". substr($kalimat_2,0,1). "<br>";
        echo "kata kedua : ". substr($kalimat_2,2,4). "<br>";
        echo "kata ketiga : ". substr($kalimat_2,7,3). "<br><br>";

        echo "<h3>Soal 3</h3>";
        $kalimat_3 = "PHP is old but sexy!";
        echo "Kalimat ketiga : ". $kalimat_3 . "<br>";
        echo "Ganti Kalimat : ". str_replace("sexy!","awesome",$kalimat_3);
        
    ?>
</body>
</html>