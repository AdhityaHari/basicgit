<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Array</h2>
    <?php
        echo "<h3>Soal 1</h3>";
        $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" ];
        print_r($kids);

        echo "<h3>Soal 2</h3>";
        echo "Total Anak : ". count($kids);
        echo "<ol>";
        echo "<li>" . $kids[0] ."</li>";
        echo "<li>" . $kids[1] ."</li>";
        echo "<li>" . $kids[2] ."</li>";
        echo "<li>" . $kids[3] ."</li>";
        echo "<li>" . $kids[4] ."</li>";
        echo "<li>" . $kids[5] ."</li>";
        echo "</ol>";

        echo "<h3>Soal 3</h3>";
        $biodata = [
            ["Nama" => "Mike", "Age" => 12, "Aliases" => "Will the Wise", "Status" => "Alive" ],
            ["Nama" => "Dustin", "Age" => 20, "Aliases" => "Dungeon Master", "Status" => "Alive" ],
            ["Nama" => "Will", "Age" => 12, "Aliases" => "Chief Hopper", "Status" => "Deceased" ],
            ["Nama" => "Lucas", "Age" => 45, "Aliases" => "El", "Status" => "Alive" ],
            ["Nama" => "Max", "Age" => 30, "Aliases" => "Will the Wise", "Status" => "Alive" ],
            ["Nama" => "Eleven", "Age" => 120, "Aliases" => "Dungeon Master", "Status" => "Alive" ],
        ];
        
        echo "<pre>";
        print_r($biodata);
        echo "</pre>";
    ?>
</body>
</html>